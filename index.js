const app = require('express')();
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const cors = require('cors');
const { point } = require('@turf/helpers');
const pointDistance = require('@turf/distance');
const isEqual = require('lodash/isEqual');

dotenv.config();

const PGUSER = (() => {
  if (!process.env.PGUSER) {
    throw new Error('Need PGUSER env var');
  }

  return process.env.PGUSER;
})();

const PGPASSWORD = (() => {
  if (!process.env.PGPASSWORD) {
    throw new Error('Need PGPASSWORD env var');
  }

  return process.env.PGPASSWORD;
})();

const PGHOST = (() => {
  if (!process.env.PGHOST) {
    throw new Error('Need PGHOST env var');
  }

  return process.env.PGHOST;
})();

const PGPORT = (() => {
  if (!process.env.PGPORT) {
    throw new Error('Need PGPORT env var');
  }

  return process.env.PGPORT;
})();

const PGDATABASE = (() => {
  if (!process.env.PGDATABASE) {
    throw new Error('Need PGDATABASE env var');
  }

  return process.env.PGDATABASE;
})();

const AMQP_SERVER = (() => {
  if (!process.env.AMQP_SERVER) {
    throw new Error('Need AMQP_SERVER env var');
  }

  return process.env.AMQP_SERVER;
})();

const AMQP_EXCHANGE = (() => {
  if (!process.env.AMQP_EXCHANGE) {
    throw new Error('Need AMQP_EXCHANGE env var');
  }

  return process.env.AMQP_EXCHANGE;
})();

const AMQP_PATTERN = (() => {
  if (!process.env.AMQP_PATTERN) {
    throw new Error('Need AMQP_PATTERN env var');
  }

  return process.env.AMQP_PATTERN;
})();

const PORT = process.env.PORT || 5000;

const MIN_DISTANCE_TO_UPDATE_LOCATION = 0.2;

const createStatesClient = require('./src/state');
const createUsersClient = require('./src/user');
const { createAMQPClient } = require('./src/amqp');
const updateHelper = require('./src/updateHelper');
const publicStateBuilder = require('./src/publicState');

app.use(bodyParser.json());

app.use(cors());

const getDistanceBetween = (p1, p2) => {
  if (!p1 || !p2) {
    return null;
  }

  const point1 = point([p1.lat, p1.lng]);
  const point2 = point([p2.lat, p2.lng]);

  return pointDistance(point1, point2);
};

const run = async () => {
  try {
    const states = await createStatesClient({
      host:     PGHOST,
      port:     PGPORT,
      user:     PGUSER,
      password: PGPASSWORD,
      database: PGDATABASE,
    });

    const users = await createUsersClient({
      host:     PGHOST,
      port:     PGPORT,
      user:     PGUSER,
      password: PGPASSWORD,
      database: PGDATABASE,
    });

    const amqpClient = await createAMQPClient(
      AMQP_SERVER,
      AMQP_EXCHANGE,
      AMQP_PATTERN
    );

    app.get('/states', async (req, res) => {
      try {
        const result = await states.getAll();

        res.status(200).json({ states: result });
      } catch(e) {
        console.error(e);
        res.sendStatus(500);
      }
    });

    app.post('/states/:userId/update', async ({ params, body }, res) => {
      try {
        const { userId } = params;
        const update = body;

        console.log('received update', update, 'for user', userId);

        if (!updateHelper.isValidUpdate(update)) {
          res.status(400).json({ error : 'Not a valid update' });
          return;
        }

        const user = await users.get(userId);

        if (!user) {
          res.status(400).json({ error : 'Not a valid user' });
          return;
        }

        const currentState = await states.get(userId);

        const updates = Object.keys(update);

        let shouldSendUpdate = true;

        // if update is only for location
        if (
          isEqual(updates, ['location'])
          && currentState
          && currentState.state.location
        ) {
          const d = getDistanceBetween(
            update.location,
            currentState.state.location
          );

          if (d < MIN_DISTANCE_TO_UPDATE_LOCATION) {
            shouldSendUpdate = false;
          }
        }

        if (
          isEqual(updates, ['currentEvent'])
          && currentState
          && currentState.state.currentEvent
        ) {
          const isSameEvent = isEqual(
            update.currentEvent,
            currentState.state.currentEvent
          );

          console.log('isSameEvent', isSameEvent);

          if (isSameEvent) {
            shouldSendUpdate = false;
          }
        }

        await states.save(userId, update);

        const state = await states.get(userId);
        const publicState = publicStateBuilder(state.state, user);

        console.log('publicState for user', userId, publicState);

        res.status(200).json(publicState);

        if (shouldSendUpdate) {
          amqpClient.send(userId, publicState);
        }
      } catch(e) {
        console.error(e);
        res.sendStatus(500);
      }
    });

    app.get('/states/:userId', async ({ params }, res) => {
      try {
        const { userId } = params;

        const user = await users.get(userId);

        if (!user) {
          res.status(400).json({ error : 'Not a valid user' });
          return;
        }

        const state = await states.get(userId);
        const publicState = publicStateBuilder(state.state, user);

        res.status(200).json(publicState);
      } catch(e) {
        console.error(e);
        res.sendStatus(500);
      }
    });

  } catch(e) {
    console.error(e);
    process.exit(-1);
  }

};

run();

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
