CREATE TABLE states (
  user_id     TEXT PRIMARY KEY,

  state       JSONB,

  created_at  TIMESTAMP DEFAULT now(),
  updated_at  TIMESTAMP DEFAULT now()
)

