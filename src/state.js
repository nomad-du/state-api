const { Client } = require('pg');

const createStatesClient = async (config) => {
  const client = new Client(config);
  try {
    await client.connect();
  } catch(e) {
    console.error(e);
    process.exit(-1);
  }

  return {
    getAll: async () => {
      const query = 'SELECT * from states';

      const res = await client.query(query);
      return res.rows ? res.rows : null;
    },
    get: async (userId) => {
      const query = `SELECT * FROM states WHERE user_id = '${userId}'`;

      const res = await client.query(query);

      return res.rows ? res.rows[0] : null;
    },
    save: async (userId, update) => {
      const query = `
        INSERT INTO states (user_id, state, updated_at)
        VALUES ($1, $2, $3)
        ON CONFLICT (user_id) DO UPDATE
          SET state = states.state || $4::jsonb,
              updated_at = $3;
      `;

      const values = [
        userId,
        update,
        new Date(),
        JSON.stringify(update),
      ];

      return client.query(query, values);
    },
  };
};

module.exports = createStatesClient;
