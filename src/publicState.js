const { point } = require('@turf/helpers');
const pointDistance = require('@turf/distance');
const minBy = require('lodash/minBy');

const MAX_DISTANCE = 1; // km

/**
 * getLocationName
 *
 * @params {Object} currentLocation
 * @params {Number} currentLocation.lat
 * @params {Number} currentLocation.lng
 * @params {Array}  locations
 * @params {Object} locations[]
 * @params {String} locations[].name
 * @params {String} locations[].type
 * @params {Number} locations[].lat
 * @params {Number} locations[].lng
 */
const getLocation = ({ lng, lat }, locations) => {
  if (!lng || ! lat) {
    return null;
  }

  if (!locations) {
    return null;
  }

  const currentPoint = point([lng, lat]);

  const distances = locations.map((location) => {
    const locationPoint = point([location.lng, location.lat]);

    const distance = pointDistance(
      locationPoint,
      currentPoint
    );

    return { location, distance };
  });

  const closestLocation = minBy(distances, 'distance');

  if (closestLocation.distance < MAX_DISTANCE) {
    return closestLocation.location;
  } else {
    return null;
  }
};

const getActivity = (event, transport, phone) => {
  if (phone) {
    return {
      type: 'phone',
      ...phone
    };
  }

  if (transport) {
    return {
      type: 'train', // TODO car, plane
      ...transport
    };
  }

  if (event) {
    return {
      type: 'meeting',
      start: event.startDate,
      end:   event.endDate,
    };
  }

  return {};
};

const publicStateBuilder = (state, user) => ({
  user_id: user.id,
  name:    user.name,
  img:     user.img,

  location: getLocation(
    state.location || {},
    user.options && user.options.locations
  ),

  activity: getActivity(
    state.currentEvent,
    state.transport,
    state.phone,
  ),

  active:       state.active,
  doNotDisturb: state.doNotDisturb,
  mood:         state.mood,

  lastUpdate: Date.now(),
});

module.exports = publicStateBuilder;
