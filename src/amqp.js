const amqplib = require('amqplib');

const createAMQPClient = async (server, exchange, topic) => {
  const connection = await amqplib.connect(`amqp://${server}`);
  const channel    = await connection.createChannel();

  await channel.assertExchange(
    exchange,
    'topic',
    { durable: false } // TODO true ?
  );

  return {
    send: (userId, update) => {
      const content = JSON.stringify({
        userId,
        update,
      });

      return channel.publish(
        exchange,
        `${topic}.${userId}`,
        Buffer.from(content)
      );
    }
  };
};

module.exports = {
  createAMQPClient,
};
