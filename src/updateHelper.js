const VALID_UPDATES = [
  'currentEvent',
  'location',
  'mood',
  'doNotDisturb',
  'active',
  'transport',
  'phone',
];

const updateHelper = {
  isValidUpdate: (update) => {
    const keys = Object.keys(update);

    const valid = keys.reduce(
      (acc, key) => acc && VALID_UPDATES.includes(key),
      true
    );

    return valid;
  }
};

module.exports = updateHelper;
