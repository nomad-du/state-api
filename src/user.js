const { Client } = require('pg');

const createUsersClient = async (config) => {
  const client = new Client(config);
  try {
    await client.connect();
  } catch(e) {
    console.error(e);
    process.exit(-1);
  }

  return {
    get: async (userId) => {
      const query = `SELECT * FROM users WHERE id = '${userId}'`;

      const res = await client.query(query);

      return res.rows ? res.rows[0] : null;
    },
  };
};

module.exports = createUsersClient;
