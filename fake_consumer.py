import sys
import pika

def callback(ch, method, properties, body):
    print(' [%r] Received %r' % (method.routing_key, body))

if __name__ == '__main__':

    topic = 'states.update.*'
    print('Topic : ', topic)

    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()

    channel.exchange_declare(
        exchange = 'states',
        exchange_type = 'topic'
    )

    new_queue = channel.queue_declare(exclusive = True)
    queue_name = new_queue.method.queue

    channel.queue_bind(
        exchange = 'states',
        queue = queue_name,
        routing_key = topic
    )

    channel.basic_qos(prefetch_count = 1)
    channel.basic_consume(
        callback,
        queue = queue_name,
        no_ack = True,
    )

    print(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()

